require("dotenv").config();
const express = require("express");
const demoLogger = require("./plugins/loggerPlugin");
const app = express();

const port = process.env.PORT || 8080;
const animeRouter = require("./routes/anime");
const forwardRouter = require("./routes/forward");

//application level plugin
app.use(demoLogger);
app.use("/anime", animeRouter);
app.use("/forward", forwardRouter);

app.listen(port, () => {
  console.log(`Running on port ${port}!`);
});
