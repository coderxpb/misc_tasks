// https://docs.api.jikan.moe/#operation/getAnimeSearch
const url = require("url");
const express = require("express");
const router = express.Router();
const needle = require("needle");

const minimalResponse = require("../plugins/minimalResponse");
const BASE_URL = process.env.ANIME_BASE_URL;

router.get("/search", async (req, res, next) => {
  const params = new URLSearchParams({ ...url.parse(req.url, true).query });

  const apiRes = await needle("get", `${BASE_URL}/anime?${params}`);
  const data = apiRes.body;

  if (data) {
    req.apiRes = data;
    next();
  } else {
    res.status(apiRes.statusCode);
  }
});

//using a plugin at router level
//this particular plugin needs to be after all the other routes since it
//modifies the raw response received
router.use(minimalResponse);

module.exports = router;
