// wasn't sure if the proxy server mentioned in the task was supposed to be like
// this where any api call could be given and I just need to forward it while
// having the ability to modify query/response
// doesn't really make much sense in terms of use cases

const url = require("url");
const express = require("express");
const router = express.Router();
const needle = require("needle");

const requestFilter = require("../plugins/requestsFilter");

//using a plugin at route level to block/allow requests
router.get("/", requestFilter, async (req, res) => {
  const parsed = url.parse(req.query.url, true);
  //can block request based on hostname, paths, hrefs, queries etc. if needed

  const apiRes = await needle("get", req.query.url);
  const data = apiRes.body;
  res.status(200).send(data);
});

module.exports = router;
