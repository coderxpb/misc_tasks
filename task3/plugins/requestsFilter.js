const url = require("url");

const blacklistDomains = ["google.com", "facebook.com", "twitter.com"];

const requestFilter = (req, res, next) => {
  const parsed = url.parse(req.query.url, true);

  const isDomainBlacklisted = blacklistDomains.some((domain) =>
    parsed.hostname.includes(domain)
  );

  //can do similar filtering for queries etc.
  if (isDomainBlacklisted) {
    res.status(403).send("Blacklisted Request");
    return;
  } else {
    next();
  }
};

module.exports = requestFilter;
