// https://codesource.io/creating-a-logging-middleware-in-expressjs/
const fs = require("fs");
const murl = require("url");

const getActualRequestDurationInMilliseconds = (start) => {
  const NS_PER_SEC = 1e9; //  convert to nanoseconds
  const NS_TO_MS = 1e6; // convert to milliseconds
  const diff = process.hrtime(start);
  return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};

const demoLogger = (req, res, next) => {
  const parsed = murl.parse(req.url, true);

  //only log api with /anime or /forward endpoints
  let folder = parsed.pathname.includes("anime")
    ? "anime"
    : parsed.pathname.includes("forward")
    ? "forward"
    : null;

  if (folder) {
    let current_datetime = new Date();
    let log_date =
      current_datetime.getFullYear() +
      "-" +
      (current_datetime.getMonth() + 1) +
      "-" +
      current_datetime.getDate();

    let formatted_date =
      current_datetime.getFullYear() +
      "-" +
      (current_datetime.getMonth() + 1) +
      "-" +
      current_datetime.getDate() +
      " " +
      current_datetime.getHours() +
      ":" +
      current_datetime.getMinutes() +
      ":" +
      current_datetime.getSeconds();

    let method = req.method;
    let url = req.url;
    let status = res.statusCode;
    const start = process.hrtime();
    const durationInMilliseconds = getActualRequestDurationInMilliseconds(start);

    let log = `[${formatted_date}] ${method}:${url} ${status} ${durationInMilliseconds.toLocaleString()} ms`;

    fs.appendFile(
      `logs/${folder}/${log_date}_request_logs.txt`,
      log + "\n",
      (err) => {
        if (err) {
          console.log(err);
        }
      }
    );
  }
  next();
};

module.exports = demoLogger;
