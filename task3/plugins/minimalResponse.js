//takes raw response from Jikan search api and only sends 3 fields to the user
//title, type and episodes

const minimalResponse = (req, res, next) => {
  const newData = req.apiRes.data.map((e) => {
    return {
      title: e.title,
      type: e.type,
      episodes: e.episodes,
    };
  });
  res.status(200).send(newData);
};

module.exports = minimalResponse;
