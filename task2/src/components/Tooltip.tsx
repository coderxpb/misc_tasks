import { ReactElement, useEffect, useState } from "react";
import ReactDOM from "react-dom";
import "./tooltip.scss";

interface PropsType {
  children?: ReactElement;
  targetID?: string;
  targetRef?: any;
  info: string;
  direction?: string;
  trigger?: "hover" | "click" | null;
  minWidth?: string;
  color?: string;
  fontSize?: string;
  backgroundColor?: string;
}

export const Tooltip = (props: PropsType) => {
  const {
    children,
    targetID,
    targetRef,
    info,
    minWidth,
    color,
    fontSize,
    backgroundColor,
  } = props;

  let { direction, trigger } = props;

  //custom styling props
  let tooltipStyle = { minWidth, color, fontSize, backgroundColor };

  direction = direction || "right";
  trigger = trigger || "hover";

  const [showTooltip, setShowTooltip] = useState(false);
  const [target, setTarget] = useState(null);

  const setTooltipVis = () => {
    setShowTooltip(true);
  };

  const setTooltipHide = () => setShowTooltip(false);
  //toggle the showTooltip state based on previous state value
  const toggleTooltip = () => setShowTooltip((prev) => !prev);

  let wrapperProps = {};
  if (trigger == "hover")
    wrapperProps = {
      onMouseEnter: setTooltipVis,
      onMouseLeave: setTooltipHide,
    };
  else if (trigger == "click") wrapperProps = { onClick: toggleTooltip };

  //if there are no composed children, try to find the target element by ref or id
  useEffect(() => {
    if (!children) {
      let targetElement = null;

      //if ref exists, set targetElement to ref
      if (targetRef && targetRef.current) targetElement = targetRef.current;
      //if ref doesn't exist but id does find targetElement by ID
      else if (targetID && !targetRef)
        targetElement = document.getElementById(targetID);

      //if targetElement does exist, add the logic to show/hide tooltip
      if (targetElement) {
        if (trigger == "hover") {
          targetElement.onmouseenter = setTooltipVis;
          targetElement.onmouseleave = setTooltipHide;
        } else if (trigger == "click") {
          targetElement.onclick = toggleTooltip;
        }

        targetElement.style.position = "relative";
        setTarget(targetElement);
      }
    }
  }, []);

  return children ? (
    //if there are children, create a wrapper which shows/hides tooltip
    <div className="wrapper" {...wrapperProps}>
      {children}
      {showTooltip && (
        <div className={"tooltip-" + "top"} style={tooltipStyle}>
          {info}
        </div>
      )}
    </div>
  ) : target && showTooltip ? (
    //otherwise if there is a target, create a portal
    ReactDOM.createPortal(
      <div className={"tooltip-" + direction} style={tooltipStyle}>
        {info}
      </div>,
      target
    )
  ) : (
    <div></div>
  );
};
