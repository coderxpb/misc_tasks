import { createRef, useRef } from "react";
import { createRoot } from "react-dom/client";
import "./App.css";
import { Tooltip } from "./components/Tooltip";
function App() {
  const htmlRef = createRef<HTMLParagraphElement>();
  const farAwayRef = createRef<HTMLButtonElement>();

  return (
    <div className="App">
      <header className="App-header">
        <Tooltip
          info="composition tooltip! with a child inside it "
          color="#F73D93"
          backgroundColor="#16003B"
          minWidth="420px"
          direction="top"
        >
          <p>Hello Vite + React!</p>
        </Tooltip>

        <span ref={htmlRef} style={{ padding: "10px" }}>
          Edit <code>App.tsx</code> and save to test HMR updates. jkdsjfk jskf
          jkasjf kdjfk alfk alskf j
        </span>
        <Tooltip
          info="wow! tooltip which takes in a ref"
          targetRef={htmlRef}
          direction={"bottom"}
          color="#FFCD38"
          backgroundColor="#187498"
        />

        <Tooltip
          info="tooltip with id"
          targetID="idButton"
          direction="left"
          backgroundColor="#97C4B8"
        />
        <button ref={farAwayRef} style={{ fontSize: "18px", marginTop: "40px" }}>
          I'm a button so click me
        </button>
        <button id="idButton" style={{ marginTop: "40px" }}>
          I'm a button with id
        </button>
        <Tooltip info="kinda far away" targetRef={farAwayRef} trigger="click" />
      </header>
    </div>
  );
}

export default App;
