//not sure if there was supposed to be some edge cases or if I misinterpreted the task, but this should remove all double quotes from any text
//it will not mutate the original string/text and will simply return back a new string after extracting quotations

//wanted to use replaceAll but turns out it's relatively new and not supported by my local node version
//const extractQuotes = (text) => text.replaceAll('"', "");

const extractQuotes = (text) => text.replace(/\"/g, "");

let string1 = 'He said "Hello."';
let string2 = '"To quote or to quote not..."';

console.log(string1, extractQuotes(string1), string1);
console.log(extractQuotes(string2));
